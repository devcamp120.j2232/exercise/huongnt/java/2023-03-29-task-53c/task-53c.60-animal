public class App {
    public static void main(String[] args) throws Exception {
        // khởi tạo 2 đối tượng Dog1, Dog2
        Dog dog = new Dog("Doggy");
        Cat cat = new Cat("Katty");
        BigDog bigDog = new BigDog("Bully");
        // in ra console
        System.out.println("Dog: " + dog.toString());
        dog.greets();
        System.out.println("Cat: " + cat.toString());
        cat.greets();

        System.out.println("BigDog: " + bigDog.toString());
        dog.greets(bigDog);
        bigDog.greets(bigDog);
    }
}
